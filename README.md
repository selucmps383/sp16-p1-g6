# Inventory Tracking Application
## Backend
  - As an administrator, I should be able to login using my username/password in order to access the back-end management portal.
      - Database must be generated and seeded using Entity Framework, Code First migrations.
          - User
              - Id
              - Username
              - First Name
              - Last Name
              - Password   
          - InventoryItem
              - Id
              - CreatedByUserId
              - Name
              - Quantity

	- When pulling your project down to present, we will run the update database command to create your database - it cannot exist beforehand.
    - Be sure to seed an `admin` user with the password `selu2014` 
    - Usernames must be unique (and enforced at the database level)
    - Passwords must be hashed (Use the [`Crypto.HashPassword`](https://msdn.microsoft.com/en-us/library/system.web.helpers.crypto.hashpassword(v=vs.111).aspx) method)
		- Code example
		
                public void SavePassword(string unhashedPassword)
                {
                  string hashedPassword = Crypto.HashPassword(unhashedPassword);
                  //Save hashedPassword somewhere that you can retrieve it again.
                  //Don't save unhashedPassword! Just let it go.
                }

    - You are not to use the “default” ASP.NET Membership Provider.
    - Verify the password that the user entered against the one stored in the database using the [`Crypto.VerifyHashedPassword`](https://msdn.microsoft.com/en-us/library/system.web.helpers.crypto.verifyhashedpassword(v=vs.111).aspx) method (lookup by `Username`).
		- Code example

                public bool CheckPassword(string unhashedPassword)
                {
                  string savedHashedPassword = //get hashedPassword from where you saved it
                  return Crypto.VerifyHashedPassword(savedHashedPassword, unhashedPassword)
                }

    - Once the user enters the correct username/password,  you can authenticate them using the [`FormsAuthentication.SetAuthCookie`](https://msdn.microsoft.com/en-us/library/twk5762b(v=vs.110).aspx) method.
    	- This should be helpful: http://www.codeproject.com/Articles/578374/AplusBeginner-splusTutorialplusonplusCustomplusF
    - Only the public login section, the inventory level viewing page, and the purchasing endpoints may be accessible when not logged in (Use the `Authorize` attribute to secure your site). 
    	- This should be helpful: http://www.codeproject.com/Articles/578374/AplusBeginner-splusTutorialplusonplusCustomplusF

  - As an administrator, I should be able to list, add, edit, and delete users from the system. 
  	- There will be a template page for this created by visual studio
  - As an administrator, I should be able to list, add, edit, and delete inventory items from the system. 
  	- There will be a template page for this created by visual studio

## Frontend
- As a user, I should be able to go to a public page without logging in, in order to see the current inventory levels, updated in real time.
	- You must use SignalR on the inventory level viewing page. When a user hits the purchase endpoint and the inventory is subtracted, an update must be pushed to the client pages that are open with updated information. 
- As a user, I should be able to submit a `GET` request to an inventory endpoint, in order to retrieve inventory levels from the system.
- As a user, I should be able to execute a `POST` request to an inventory endpoint, in order to purchase an item (and decrease the inventory level). 