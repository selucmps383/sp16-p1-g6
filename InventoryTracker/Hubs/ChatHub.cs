﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace InventoryTracker.Hubs
{
    public class ChatHub : Hub
    {
        public void BroadcastMessage(string message)
        {
            Clients.All.writeMessage(message);
        }
    }
}