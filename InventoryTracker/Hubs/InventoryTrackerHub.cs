﻿using InventoryTracker.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InventoryTracker
{
    public class InventoryTrackerHub : Hub
    {
        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<InventoryTrackerHub>();

        // Call this from JS: hub.client.send(channel, content)
        public void Send(string channel, string content)
        {
            Clients.All.addMessage(content);
        }

        // Call this from C#: NewsFeedHub.Static_Send(channel, content)
        public static void Static_Send(string content)
        {
            hubContext.Clients.All.addMessage(content);
        }
            
        public static void ButtonPress()
        {
            hubContext.Clients.All.buttonPress();
        }
        public static void UpdateInventory(InventoryItem item)
        {
            hubContext.Clients.All.UpdateInventory(item);
        }

    }
}