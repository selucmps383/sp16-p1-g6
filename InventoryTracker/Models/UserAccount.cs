﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace InventoryTracker.Models
{
    public class UserAccount : IValidatableObject
    {
        [Key]
        public int UserId { get; set; }

        [MaxLength(13)]
        [Required(ErrorMessage = "First Name is required.")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [MaxLength(15)]
        [Required(ErrorMessage = "Last Name is required.")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [MaxLength(13)]
        [Required(ErrorMessage = "Username is required.")]
        [DisplayName("User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DisplayName("Enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required.")]
        [DisplayName("Confirm Password")]
        [DataType(DataType.Password)]
        [NotMapped]
        public string ConfirmPassword { get; set; }

        public string UserState { get; set; }

        public string UserRole { get; set; }



        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            InventoryTrackerContext db = new InventoryTrackerContext();
            if (db.UserAccounts.Where(p => p.UserName == this.UserName && p.UserId != this.UserId).Any())
            {
                yield return new ValidationResult("Username already exist!", new[] { "UserName" });
            }

        }



    }

}