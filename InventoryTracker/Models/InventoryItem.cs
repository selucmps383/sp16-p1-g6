﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryTracker.Models
{
    public class InventoryItem
    {
        public int Id { get; set; }
        [JsonIgnore]
        public int CreatedByUserID { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}