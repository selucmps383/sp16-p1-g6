﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InventoryTracker.Models
{
    public class InventoryTrackerContext : DbContext
    {

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserAccount>().Ignore(t => t.ConfirmPassword);
            base.OnModelCreating(modelBuilder);
        }
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public InventoryTrackerContext() : base("name=InventoryTrackerContext")
        {
        }

        public System.Data.Entity.DbSet<InventoryTracker.Models.UserAccount> UserAccounts { get; set; }
        public System.Data.Entity.DbSet<InventoryTracker.Models.InventoryItem> InventoryItem { get; set; }
    }
}
