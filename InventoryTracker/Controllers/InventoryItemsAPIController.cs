﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using InventoryTracker.Models;

namespace InventoryTracker.Controllers
{
    public class InventoryItemsAPIController : ApiController
    {
        private InventoryTrackerContext db = new InventoryTrackerContext();

        // GET: api/InventoryItemsAPI
        public IQueryable<InventoryItem> GetInventoryItem()
        {
            return db.InventoryItem;
        }

        // GET: api/InventoryItemsAPI/5
        [ResponseType(typeof(InventoryItem))]
        public IHttpActionResult GetInventoryItem(int id)
        {
            InventoryItem inventoryItem = db.InventoryItem.Find(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return Ok(inventoryItem);
        }

        //// PUT: api/InventoryItemsAPI/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutInventoryItem(int id, InventoryItem inventoryItem)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != inventoryItem.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(inventoryItem).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!InventoryItemExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/InventoryItemsAPI
        [ResponseType(typeof(InventoryItem))]
        public IHttpActionResult PostInventoryItem(int id)
        {
            InventoryItem inventoryItem = db.InventoryItem.Find(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }
            inventoryItem.Quantity--;
            db.Entry(inventoryItem).State = EntityState.Modified;
            db.SaveChanges();

            InventoryTrackerHub.UpdateInventory(inventoryItem);

            return CreatedAtRoute("DefaultApi", new { id = inventoryItem.Id }, inventoryItem);
        }

        //// DELETE: api/InventoryItemsAPI/5
        //[ResponseType(typeof(InventoryItem))]
        //public IHttpActionResult DeleteInventoryItem(int id)
        //{
        //    InventoryItem inventoryItem = db.InventoryItem.Find(id);
        //    if (inventoryItem == null)
        //    {
        //        return NotFound();
        //    }

        //    db.InventoryItem.Remove(inventoryItem);
        //    db.SaveChanges();

        //    return Ok(inventoryItem);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InventoryItemExists(int id)
        {
            return db.InventoryItem.Count(e => e.Id == id) > 0;
        }
    }
}