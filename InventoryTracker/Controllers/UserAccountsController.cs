﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryTracker.Models;
using System.Web.Helpers;
using System.Web.Security;

namespace InventoryTracker.Controllers
{
    //Require authentication for all pages (in this controller) unless otherwise specified
    [Authorize]
    public class UserAccountsController : Controller
    {
        private InventoryTrackerContext db = new InventoryTrackerContext();

        //TODO: Use a separate view model and database model? (because the unencrypted confirm password is being stored)
        //TODO: Add database side enforcement of unique usernames
        [HttpPost,AllowAnonymous]
        public ActionResult Register(UserAccount account)
        {
            if (ModelState.IsValid)
            {
                using (InventoryTrackerContext db = new InventoryTrackerContext())
                {
                    //TODO: Remove this and use the model to check it when the new database model is added
                    if (!account.Password.Equals(account.ConfirmPassword))
                    {
                        ModelState.AddModelError("ConfirmPassword", "Passwords don't match");
                        return View();
                    }
                    account.Password = Crypto.HashPassword(account.Password);
                    db.UserAccounts.Add(account);
                    db.SaveChanges();
                }
                ModelState.Clear();
                ViewBag.Message = account.FirstName + " " + account.LastName + " successfully registered.";
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost,AllowAnonymous]
        public ActionResult Login(UserAccount user)
        {
            using (InventoryTrackerContext db = new InventoryTrackerContext())
            {
                String hashedPassword = Crypto.HashPassword(user.Password);
                var usr = db.UserAccounts.Where(u => u.UserName == user.UserName).FirstOrDefault();

                if (usr != null) {
                    bool checkHashedPassword = Crypto.VerifyHashedPassword(usr.Password, user.Password);
                    if (checkHashedPassword == true)
                    {
                        Session["UserID"] = usr.UserId;
                        Session["Username"] = usr.UserName.ToString();
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                            usr.UserName.ToString(),
                            DateTime.Now,
                            DateTime.Now.AddDays(30),
                            true,
                            usr.UserId.ToString(),
                        FormsAuthentication.FormsCookiePath);

                        string encTicket = FormsAuthentication.Encrypt(ticket);

                        
                        Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));


                        //FormsAuthentication.SetAuthCookie(usr.UserName.ToString(), false);
                        //TODO: Redirect to ReturnUrl
                        return Redirect("LoggedIn");
                    }
                }
                ModelState.AddModelError("", "Username or Password is wrong.");
            }
            return View();
        }

        public ActionResult LoggedIn()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            FormsAuthentication.SignOut();
            ModelState.AddModelError(" ", "Successfully Logged Out.");
            return View("Login");
        }

        // GET: UserAccounts
        public ActionResult Index()
        {
            return View(db.UserAccounts.ToList());
        }

        // GET: UserAccounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAccount userAccount = db.UserAccounts.Find(id);
            if (userAccount == null)
            {
                return HttpNotFound();
            }
            return View(userAccount);
        }

        // GET: UserAccounts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserId,FirstName,LastName,UserName,Password,ConfirmPassword,UserState,UserRole")] UserAccount userAccount)
        {
            if (ModelState.IsValid)
            {
                db.UserAccounts.Add(userAccount);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userAccount);
        }

        // GET: UserAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAccount userAccount = db.UserAccounts.Find(id);
            if (userAccount == null)
            {
                return HttpNotFound();
            }
            return View(userAccount);
        }

        // POST: UserAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserId,FirstName,LastName,UserName,Password,ConfirmPassword,UserState,UserRole")] UserAccount userAccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userAccount);
        }

        // GET: UserAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAccount userAccount = db.UserAccounts.Find(id);
            if (userAccount == null)
            {
                return HttpNotFound();
            }
            return View(userAccount);
        }

        // POST: UserAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserAccount userAccount = db.UserAccounts.Find(id);
            db.UserAccounts.Remove(userAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
