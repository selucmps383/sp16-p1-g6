﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryTracker.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            InventoryTrackerHub.Static_Send("hellooooo");

            return View();
        }

        public ActionResult Test()
        {
            ViewBag.Title = "Test Page";

            return View();
        }
        public ActionResult Inventory()
        {
            ViewBag.Title = "Inventory";

            return View();
        }
    }
    public class ButtonModel
    {
        // We declare Left and Top as lowercase with 
        // JsonProperty to sync the client and server models
        [JsonProperty("text")]
        public string text { get; set; }
        // We don't want the client to get the "LastUpdatedBy" property
        //[JsonIgnore]
        //public string LastUpdatedBy { get; set; }
    }
}
