namespace InventoryTracker.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Helpers;
    using InventoryTracker.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<InventoryTracker.Models.InventoryTrackerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
           
        }

        protected override void Seed(InventoryTracker.Models.InventoryTrackerContext context)
        {


            string Password = Crypto.HashPassword("selu2014");

            UserAccount Admin = new UserAccount()
            {
                FirstName = "Admin",
                LastName = "User",
                UserName = "Admin",
                Password = Password,
                ConfirmPassword = Password,
                UserRole = "Admin",
                UserState = ""
            };

            InventoryItem[] TestData = new InventoryItem[]
            {
                new InventoryItem
                {
                    Name = "Trumps Hairpiece",
                    Quantity = 21
                },
                new InventoryItem
                {
                    Name = "Bernie's Pants",
                    Quantity = 21
                },
                new InventoryItem
                {
                    Name = "Hilary's Sweater",
                    Quantity = 21
                }
            };

            context.InventoryItem.AddRange(TestData);
            context.UserAccounts.Add(Admin);
            base.Seed(context);
        }
        
    }
}
